#!/usr/bin/env python3

import os
import sys
import subprocess
from distutils.dir_util import copy_tree
import shutil

if len(sys.argv) < 1:
    raise Exception('Missing argument: please provide the path to the target diectory')

if len(sys.argv) > 1 and  sys.argv[1] == '-help':
    print('You need to provide exactly one argument: the path to the directory that contains the directories that are to be emptied.')
    sys.exit()

if os.path.isdir(sys.argv[1]):
    source_path = sys.argv[1]
else:
    raise Exception('Given argument is not a directory')

print('Searching for subdirectories...')

subdirs = [dir for dir in os.listdir(source_path) if os.path.isdir(os.path.join(source_path, dir))]

subdirs[:] = [dir for dir in subdirs if not dir[0] == '.']



for dir in subdirs:
    abs_path = os.path.join(source_path, dir)
    for filename in os.listdir(abs_path):
        file_path = os.path.join(abs_path, filename)
    try:
        if os.path.isfile(file_path) or os.path.islink(file_path):
            os.unlink(file_path)
            print('Deleted content of ' + dir)
        elif os.path.isdir(file_path):
            shutil.rmtree(file_path)
    except Exception as e:
        print('Failed to delete %s. Reason: %s' % (file_path, e))

print('Done!')

