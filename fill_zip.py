#!/usr/bin/env python3

import os
import sys
import subprocess
from distutils.dir_util import copy_tree
import shutil

if len(sys.argv) > 1 and  sys.argv[1] == '-help':
    print('You may provide the following arguments: source_dir target.zip [target_name]\n')
    print('\tsource_dir: path to the directory containing the directories to copy the content from\n')
    print('\ttarget.zip: path to the zip containing the directories content will be copied to\n')
    print('\ttarget_name [optional]: the name of the resulting zip')
    sys.exit()

if len(sys.argv) < 3:
    raise Exception('Not enough arguments: Supply source path as first and target zip file as second argument')

if os.path.isdir(sys.argv[1]):
    source_path = sys.argv[1]
else:
    raise Exception('First argument is not a directory')

if sys.argv[2].endswith('.zip'):
    target_path = sys.argv[2][0:len(sys.argv[2]) - 4]
else:
    raise Exception('Second argument is not a zip file')


print('Searching for subdirectories...')

subdirs = [dir for dir in os.listdir(source_path) if os.path.isdir(os.path.join(source_path, dir))]

subdirs[:] = [dir for dir in subdirs if not dir[0] == '.']


print('Unzipping target into ' + target_path + '...')

cmd = ['unzip', sys.argv[2]]
sp = subprocess.Popen(cmd, stderr=subprocess.STDOUT, stdout=subprocess.PIPE)
sp.communicate()


for dir in subdirs:
    abs_path = os.path.join(target_path, dir)
    if os.path.isdir(abs_path):
        copy_tree(os.path.join(source_path, dir), abs_path)
        print('Copied contents of ' + dir)
    else:
        print('Couldn\'t find directory ' + dir + ' in target')


if len(sys.argv) > 3:
    target_path = sys.argv[3]

target_dir = 'filled_zips'
if not os.path.exists(target_dir):
    os.makedirs(target_dir)

print('Zipping again to '  + target_dir + '/' + target_path + '...')

cmd = ['zip', '-r', os.path.join(target_dir, target_path) + '.zip', target_path]
sp = subprocess.Popen(cmd, stderr=subprocess.STDOUT, stdout=subprocess.PIPE)
sp.communicate()


print('Removing unzipped taget...')
shutil.rmtree(target_path)

print('Done!')

